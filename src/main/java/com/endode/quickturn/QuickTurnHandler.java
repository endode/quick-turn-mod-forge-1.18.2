package com.endode.quickturn;

import net.minecraftforge.eventbus.api.SubscribeEvent;
import net.minecraftforge.api.distmarker.Dist;
import net.minecraftforge.fml.common.Mod;
import net.minecraftforge.event.TickEvent.ClientTickEvent;
import net.minecraftforge.event.TickEvent;
import net.minecraft.client.player.LocalPlayer;
import net.minecraft.client.Minecraft;
import org.slf4j.Logger;

@Mod.EventBusSubscriber(modid = "quickturn", bus = Mod.EventBusSubscriber.Bus.FORGE, value = Dist.CLIENT)
public class QuickTurnHandler {
	@SubscribeEvent
	public static void onClientTick(ClientTickEvent event) {
		if(event.phase == TickEvent.Phase.END) {
			if(QuickTurn.quickturn_key.consumeClick()) {
				LocalPlayer player = Minecraft.getInstance().player;
				player.setYRot(player.getYRot() + 180);
			}
		}
	}
}
