package com.endode.quickturn;

import com.mojang.logging.LogUtils;
import net.minecraftforge.common.MinecraftForge;
import net.minecraftforge.event.RegistryEvent;
import net.minecraftforge.eventbus.api.SubscribeEvent;
import net.minecraftforge.fml.InterModComms;
import net.minecraftforge.fml.common.Mod;
import net.minecraftforge.fml.event.lifecycle.FMLCommonSetupEvent;
import net.minecraft.client.KeyMapping;
import net.minecraftforge.event.server.ServerStartingEvent;
import net.minecraftforge.fml.javafmlmod.FMLJavaModLoadingContext;
import net.minecraftforge.client.ClientRegistry;
import org.slf4j.Logger;

import java.util.stream.Collectors;

// The value here should match an entry in the META-INF/mods.toml file
@Mod("quickturn")
public class QuickTurn {
    public static final Logger LOGGER = LogUtils.getLogger();

    public QuickTurn() {
        // Register the setup method for modloading
        FMLJavaModLoadingContext.get().getModEventBus().addListener(this::setup);

        // Register ourselves for server and other game events we are interested in
        MinecraftForge.EVENT_BUS.register(this);
    }

    public static KeyMapping quickturn_key = new KeyMapping("key.quickturn.quickturn", 82, "category.quickturn.keycat");

    private void setup(final FMLCommonSetupEvent event) {
	ClientRegistry.registerKeyBinding(quickturn_key);
        MinecraftForge.EVENT_BUS.register(new QuickTurnHandler());
    }

    @SubscribeEvent
    public void onServerStarting(ServerStartingEvent event) {
	LOGGER.warn("Quick Turn is meant to be ran on the client only, if you have it installed on the server itself then it won't do anything");
    }

}
